fun main() {

    var birthdate = mutableMapOf<String, Any>()
    var flag = false
    //To accept input from user
    while (!flag){
        if (birthdate.size/2 > 9) flag = true // To check if the user input is already 10 names
        else{
            print("\nEnter name (enter ZZZ to quit): " +
                    "")
            var name = readLine()!!
            if (name=="ZZZ") flag = true // To check if the input is ZZZ to exit
            else{
                print("Enter date of birth (dd-mm-yyyy): ")
                var dateOfBirth = readLine()!!
                birthdate["name${birthdate.size/2}"] = name // To add the data input to the map
                birthdate["birthdate${birthdate.size/2}"] = dateOfBirth  // To add the data input to the map
            }

        }
    }

    //To print how many names were entered
    var listLength=birthdate.size/2
    println("\n------------------------")
    println("Count of Names = $listLength \n")
    //to display the names and birthdate were entered
    var displayFlag = false
    var ctr = 0
    while (!displayFlag){
        if (ctr + 1 <= birthdate.size /2) println("Name: ${birthdate.getValue("name$ctr")} - Birthdate: ${birthdate.getValue("birthdate${ctr}")}")
        else displayFlag = true
        ctr++ //It serves as the iterator
    }

    var repeat = true
    var found = false
    // to check if its found and continue to ask for input until it exit using "ZZZ"
    while (repeat) {
        print("\nEnter name to searh(enter ZZZ to quit) : ")
        var searchName = readLine()
        if (searchName == "ZZZ") repeat = false
        else {
            var foundBirthdate=""
            var i = 0
            while (i < listLength && !found) {
                if (birthdate["name$i"] == searchName) { //To check if the list has the search name
                    found = true
                    foundBirthdate = birthdate["birthdate$i"].toString()
                }
                i++ //It serves as the iterator
            }
            if (found) println("Date of Birth: $searchName is $foundBirthdate") else println("Date Of Birth of $searchName is not found")
        }
        found = false
    }
}



