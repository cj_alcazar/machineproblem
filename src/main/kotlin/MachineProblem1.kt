fun main() {

    println(checkString("hotdogs"))
    println(checkString("hotdog"))

}

fun checkString(str: String): String {
    return if(str.length <= 15){ // to check if the str is <=15
        if (str.length % 2 == 0) str.reversed() // To check if its even number and return reversed string
        else String(str.toCharArray().sorted().toCharArray())// if its odd number it will sorted the string to alphabetical
    } else "Sorry, you have entered an invalid input! " //
}