fun main() {
    val question = arrayOf(
        "Question" to "What is my favorite hobby?",
        "Choices" to mapOf(
            "A" to "Eating",
            "B" to "Sleeping",
            "C" to "Gaming"
        ),
        "Question" to "What is my favorite food?",
        "Choices" to mapOf(
            "A" to "Salmon",
            "B" to "Chicken Wings",
            "C" to "Hotdog"
        ),
        "Question" to "What is my favorite dessert?",
        "Choices" to mapOf(
            "A" to "Ginataang Kamote",
            "B" to "Strawberry",
            "C" to "Chocolate"
        ),
        "Question" to "What is my favorite fruits?",
        "Choices" to mapOf(
            "A" to "Jackfruit",
            "B" to "Apple",
            "C" to "Orange"
        ),
        "Question" to "What is my favorite genre of series?",
           "Choices" to mapOf(
            "A" to "Romance",
            "B" to "Horror",
            "C" to "Suspence"
        ),
        "Question" to "What is my favorite food cuisine?",
           "Choices" to mapOf(
            "A" to "Chinese Cuisine",
            "B" to "Korean Cuisine",
            "C" to "Thai Cuisine"
        ),
        "Question" to "What is my favorite color?",
           "Choices" to mapOf(
            "A" to "Violet",
            "B" to "Pink",
            "C" to "Black and Gray"
        ),
        "Question" to "What is my favorite programming language?",
           "Choices" to mapOf(
            "A" to "ReactJS",
            "B" to "Flutter",
            "C" to "Javascript"
        ),
        "Question" to "What is my favorite things to do when i'm bored?",
           "Choices" to mapOf(
            "A" to "Laugh",
            "B" to "Annoy people",
            "C" to "Cry"
        ),
        "Question" to "What is my favorite chicken wings flavor?",
        "Choices" to mapOf(
            "A" to "Yangnyeom",
            "B" to "Chicken Buffalo",
            "C" to "Soy garlic"
        ),
    )
    val answer = arrayOf(
        "A",
        "B",
        "C",
        "A",
        "A",
        "B",
        "C",
        "A",
        "B",
        "C")

        var score=0 // to count the score
        var ctr=0 // this serves as my iterator for question array
        for(i in answer) {
            // To print the question and the choices
            println(question[ctr].second)
            println(question[ctr+1].second)
                var flag = false
                while (!flag){ // looping to keep the asking the user for a valid input
                    print("Your answer is: ")
                    val userInput = readLine()!!
                    flag = if (userInput == "A" ||userInput == ("B")||userInput == ("C")){ // to check if the user entered a valid input
                        if(userInput == i ) {
                            println("-------Correct!-------\n")
                            score++
                        } //To add score everytime the user has correct answer
                        else println("The correct answer is: $i \n") //To print the correct answer
                        true // Used to trigger the looping to stop asking for a user input
                    } else false // To keep the prompting to ask for a valid input
                }
            ctr+=2
        }
    println("_______________________________________")
    println("Your score is: $score")
    println("Your incorrect answer is: ${10-score}")
}

