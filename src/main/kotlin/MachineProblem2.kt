fun main() {
    var colors = listOf("red", "blue", "blue", "pink")
    println(colorString(colors))
}

fun colorString(colors: List<Any>): Map<Any, Int> {

    var colorRainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    var finalColors = mutableListOf<Any>()

    colors.forEach { color ->
        if (colorRainbow.contains(color.toString().lowercase())) { // To check the colorRainbow list with roygbiv if the list is found in that list
            finalColors.add(color.toString().lowercase()) // To add in the finalColors
        } else finalColors.add("Others") //If the color is not found in the colorRainbow list it will be added as "Others"
    }

    return finalColors.groupingBy { it }.eachCount() //To group all the duplicate values of the list the count each values
}