fun main(){
    var flag = 0

    var phonebook: MutableMap<String, String> = mutableMapOf(
        "Paul" to "09663091239",
        "Inday" to "09357832585",
        "Magadia" to "09027632938",
        "Kaycee" to "09328240392",
        "Juveil" to "09669402847",
        "Vynther" to "09177337379",
        "Rose" to "09169842585",
        "Jisoo" to "09996437938",
        "Jennie" to "09058967392",
        "Lisa" to "09433217854"
    )

    while(flag!=1){ // to keep asking the user for an input
        if(phonebook.size > 31)  println("Sorry, You are not allowed to add new entry!") // if the phonebook size > 31 it will not add any entries
        else {
            print("Enter a name: ")
            var userInput = readLine()!!
            if (userInput == "Quit") flag = 1 // to check if the user input is "Quit" to exit
            else checkPhonebook(phonebook, userInput) // to call the function checkPhonebook and check its number
        }
    }
}

fun checkPhonebook(phonebook: MutableMap<String,String>, contactName: String){
    if (phonebook.containsKey(contactName)) println("The contact number is: ${phonebook.getValue(contactName)}") // to print the contact number of the search user
    else { // if its not on the list the user has to add a number to phonebook
        print("Enter a phone number: ")
        var userInput = readLine()!!
        phonebook[contactName] = userInput //to add the number to the phonebook
        println("Successfully added $contactName with a number of $userInput to the phonebook!")
    }
}